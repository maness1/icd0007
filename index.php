<?php
session_start();
$_SESSION['error'] = false;
require_once 'lib/tpl.php';

$connection = new PDO('sqlite:db4.sqlite');
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$cmd = get_value('cmd') ? get_value('cmd') : 'names';  // if cmd is not set, show names table

$users = [];
$render = [];  // for render template

class User{
    public $first_name;
    public $last_name;
    public $phones;
    public $id;

    public function __construct($first_name, $last_name, $phones, $id) {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->phones = $phones;
        $this->id = $id;
    }
}

if ($cmd === 'save'){

    $_SESSION['firstName'] = get_value('firstName');
    $_SESSION['lastName'] = get_value('lastName');
    $_SESSION['phone1'] = get_value('phone1');
    $_SESSION['phone2'] = get_value('phone2');
    $_SESSION['phone3'] = get_value('phone3');
    if(strlen(get_value('firstName')) > 2 && strlen(get_value('lastName')) > 2) {
        $fistName = get_value('firstName');
        $lastName = get_value('lastName');
        $phone1 = get_value('phone1');
        $phone2 = get_value('phone2');
        $phone3 = get_value('phone3');

        $stmt = $connection->prepare( "INSERT INTO names (firstName, lastName)".
            " VALUES (?,?)");
        $stmt->execute([$fistName, $lastName]);
        //$stmt = $connection->prepare("SELECT max(id) FROM names");
        $id = $connection->lastInsertId('id');

        $stmt = $connection->prepare( "INSERT INTO phones1 (contact_id, phone)".
            " VALUES ('$id','$phone1')");
        $stmt->execute();

        $stmt = $connection->prepare( "INSERT INTO phones1 (contact_id, phone)".
            " VALUES ('$id','$phone2')");
        $stmt->execute();

        $stmt = $connection->prepare( "INSERT INTO phones1 (contact_id, phone)".
            " VALUES ('$id','$phone3')");
        $stmt->execute();
        header('Location: ?cmd=names&msg=success');
    } else {
        header('Location: ?cmd=add&msg=error');
    }

} else if ($cmd === 'names') {
    $_SESSION = array();
    $stmt = $connection->prepare( 'SELECT firstName, lastName, id FROM names');
    $stmt->execute();

    foreach($stmt as $row) {

        $stmt1 = $connection->prepare("SELECT phone FROM phones1 WHERE contact_id=?");
        $stmt1->execute([$row['id']]);
//        $a = $stmt1->fetch(PDO::FETCH_NUM);
//
//        $number_str = '';
//        $number_str = implode(', ', array_filter($a));  // array to string
        $phones = '';
        foreach($stmt1 as $number) {
            if(strlen($number['phone']) > 0) {
                $phones .= $number['phone'] . ", ";
            }
        }
        if(strlen($phones) > 2){
            $phones = substr($phones, 0, strlen($phones) - 2);
        }

        array_push($users, new User($row['firstName'], $row['lastName'], $phones, $row['id']));  // filling the array
    }

    $render['$template'] = 'data.html';  // list
    $render['$users'] = $users;  // set list of users
    if(get_value("msg") === "success")
    {
        $render['$msg'] = "Added successfully!";
    } else if(get_value("msg") === "updated"){
        $render['$msg'] = "Updated successfully!";
    }
    print render_template('template.html', $render);  // tpl renders the template
} else if($cmd === 'add'){
    $render['$template'] = 'add.html';  // add form
    $render['$action'] = 'save';
    if(get_value('msg') === 'error'){
        $render['$msg'] = "Error. First and Last names must have 2 or more symbols";
    }
    $render['$fname'] = get_session_value('firstName');
    $render['$sname'] = get_session_value('lastName');

    $render['$phone1'] = get_session_value('phone1');
    $render['$phone2'] = get_session_value('phone2');
    $render['$phone3'] = get_session_value('phone3');

    print render_template('template.html', $render);  // tpl renders the template
} else if($cmd === 'change') {
    $id = 0;
    if(get_value('msg') == '') {
        $id = get_value('id');
        $stmt1 = $connection->prepare("SELECT firstName,lastName FROM names WHERE id=?");
        $stmt1->execute([$id]);
        foreach ($stmt1 as $row) {
            $render['$fname'] = $row['firstName'];
            $render['$sname'] = $row['lastName'];
        }

        $stmt1 = $connection->prepare("SELECT phone FROM phones1 WHERE contact_id=?");
        $stmt1->execute([$id]);
        $phones = '';
        $i = 1;
        foreach ($stmt1 as $number) {
            if (strlen($number['phone']) > 0) {
                $render['$phone' . $i] = $number['phone'];
                $i = $i + 1;
            }
        }
    } else if(get_value('msg') === 'error'){
        $render['$fname'] = get_session_value('firstName');
        $render['$sname'] = get_session_value('lastName');

        $render['$phone1'] = get_session_value('phone1');
        $render['$phone2'] = get_session_value('phone2');
        $render['$phone3'] = get_session_value('phone3');
        $render['$msg'] = 'Error. First and Last names must have 2 or more symbols';
        $id = get_session_value('id');
    }
    $render['$action'] = 'update&id='.$id;
    $render['$template'] = 'add.html';
    //
    print render_template('template.html', $render);  // tpl renders the template
} else if ($cmd === 'update'){
    $_SESSION['firstName'] = get_value('firstName');
    $_SESSION['lastName'] = get_value('lastName');
    $_SESSION['phone1'] = get_value('phone1');
    $_SESSION['phone2'] = get_value('phone2');
    $_SESSION['phone3'] = get_value('phone3');
    $id = get_value('id');
    if(strlen(get_value('firstName')) > 2 && strlen(get_value('lastName')) > 2) {
        $fistName = get_value('firstName');
        $lastName = get_value('lastName');

        $phone1 = get_value('phone1');
        $phone2 = get_value('phone2');
        $phone3 = get_value('phone3');

        $stmt = $connection->prepare( "UPDATE names SET firstName=?, lastName=? WHERE id=?;");
        $stmt->execute([$fistName, $lastName, $id]);
        //$stmt = $connection->prepare("SELECT max(id) FROM names");
        //$id = $connection->lastInsertId('id');

        $stmt = $connection->prepare( "DELETE FROM phones1 WHERE contact_id='$id';");
        $stmt->execute();

        $stmt = $connection->prepare( "INSERT INTO phones1 (contact_id, phone)".
            " VALUES ('$id','$phone1')");
        $stmt->execute();

        $stmt = $connection->prepare( "INSERT INTO phones1 (contact_id, phone)".
            " VALUES ('$id','$phone2')");
        $stmt->execute();

        $stmt = $connection->prepare( "INSERT INTO phones1 (contact_id, phone)".
            " VALUES ('$id','$phone3')");
        $stmt->execute();
        $_SESSION['error'] = false;
        header('Location: ?cmd=names&msg=updated');
    } else {
        $_SESSION['error'] = true;
        $_SESSION['id'] = $id;
        header("Location: ?cmd=change&id='$id'&msg=error");
    }

}

function get_value($key) {
    if (isset($_GET[$key])) {  // if _GET is set, return its value
        return $_GET[$key];
    } else if (isset($_POST[$key])) {  // if _POST is set, return its value
        return $_POST[$key];
    } else {
        return '';
    }
}

function get_session_value($key) {
    if (isset($_SESSION[$key])) {  // if _GET is set, return its value
        return $_SESSION[$key];
    } else {
        return '';
    }
}